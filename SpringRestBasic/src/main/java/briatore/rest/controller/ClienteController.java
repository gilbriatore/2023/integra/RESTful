package briatore.rest.controller;

import briatore.rest.entity.Cliente;
import jakarta.annotation.PostConstruct;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ClienteController {

    private List<Cliente> clientes;

    @GetMapping("/clientes")
    public List<Cliente> getClientes() {
        return clientes;
    }

    @GetMapping("/clientes/{id}")
    public Cliente getCliente(@PathVariable Integer id) {
        Cliente cliente = null;
        for (Cliente c : clientes) {
            if (c.getId() == id) {
                cliente = c;
                break;
            }
        }
        return cliente;
    }

    @PostMapping("/clientes")
    public Cliente addCliente(@RequestBody Cliente cliente) {
        clientes.add(cliente);
        return cliente;
    }

    @PutMapping("/clientes")
    public Cliente updateCliente(@RequestBody Cliente cliente) {
        for (Cliente c : clientes) {
            if (c.getId() == cliente.getId()) {
                c.setNome(cliente.getNome());
                c.setCPF(cliente.getCPF());
                break;
            }
        }
        return cliente;
    }

    @DeleteMapping("/clientes/{id}")
    public List<Cliente> deleteCliente(@PathVariable Integer id) {
        for (Cliente c : clientes) {
            if (c.getId() == id) {
                clientes.remove(c);
                break;
            }
        }
        return clientes;
    }


    @PostConstruct
    public void populateClientes() {
        clientes = new ArrayList<>();
        clientes.add(new Cliente(1, "João", "123456789"));
        clientes.add(new Cliente(2, "Pedro", "987654321"));
        clientes.add(new Cliente(3, "Maria", "123987456"));
    }
}
