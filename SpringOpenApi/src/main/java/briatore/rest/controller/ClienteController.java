package briatore.rest.controller;

import briatore.rest.entity.Cliente;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.PostConstruct;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@Tag(name = "Cliente", description = "API de Clientes")
public class ClienteController {

    private List<Cliente> clientes;

    @Operation(summary = "Lista todos os clientes" ,
            description = "Lista todos os clientes cadastrados" ,
            responses = {
             @ApiResponse(responseCode = "200", description = "Lista de clientes")
    })
    @GetMapping("/clientes")
    public List<Cliente> getClientes() {
        return clientes;
    }


    @Operation(summary = "Busca um cliente pelo ID" ,
            description = "Busca um cliente pelo ID" ,
            responses = {
                @ApiResponse(responseCode = "200", description = "Cliente encontrado"),
                @ApiResponse(responseCode = "404", description = "Cliente não encontrado")
    })
    @GetMapping("/clientes/{id}")
    public Cliente getCliente(@Parameter(description = "ID do cliente",
            required = true, example = "1") @PathVariable Integer id) {
        Cliente cliente = null;
        for (Cliente c : clientes) {
            if (c.getId() == id) {
                cliente = c;
                break;
            }
        }
        return cliente;
    }

    @Operation(summary = "Adiciona um cliente" ,
            description = "Adiciona um cliente" ,
            responses = {
                @ApiResponse(responseCode = "201", description = "Cliente adicionado"),
                @ApiResponse(responseCode = "400", description = "Erro na requisição")
    })
    @PostMapping("/clientes")
    public Cliente addCliente(@RequestBody Cliente cliente) {
        clientes.add(cliente);
        return cliente;
    }

    @Operation(summary = "Atualiza um cliente" ,
            description = "Atualiza um cliente" ,
            responses = {
                @ApiResponse(responseCode = "200", description = "Cliente atualizado"),
                @ApiResponse(responseCode = "400", description = "Erro na requisição")
    })
    @PutMapping("/clientes")
    public Cliente updateCliente(@RequestBody Cliente cliente) {
        for (Cliente c : clientes) {
            if (c.getId() == cliente.getId()) {
                c.setNome(cliente.getNome());
                c.setCPF(cliente.getCPF());
                break;
            }
        }
        return cliente;
    }

    @Operation(summary = "Deleta um cliente" ,
            description = "Deleta um cliente" ,
            responses = {
                @ApiResponse(responseCode = "200", description = "Cliente deletado"),
                @ApiResponse(responseCode = "400", description = "Erro na requisição")
    })
    @DeleteMapping("/clientes/{id}")
    public List<Cliente> deleteCliente(@PathVariable Integer id) {
        for (Cliente c : clientes) {
            if (c.getId() == id) {
                clientes.remove(c);
                break;
            }
        }
        return clientes;
    }


    @PostConstruct
    public void populateClientes() {
        clientes = new ArrayList<>();
        clientes.add(new Cliente(1, "João", "123456789"));
        clientes.add(new Cliente(2, "Pedro", "987654321"));
        clientes.add(new Cliente(3, "Maria", "123987456"));
    }
}
